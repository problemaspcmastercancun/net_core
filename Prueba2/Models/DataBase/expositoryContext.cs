﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Prueba2.Models.DataBase
{
    public partial class expositoryContext : DbContext
    {
        public expositoryContext()
        {
        }

        public expositoryContext(DbContextOptions<expositoryContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Calificaciones> Calificaciones { get; set; }
        public virtual DbSet<Evaluaciones> Evaluaciones { get; set; }
        public virtual DbSet<Evaluadores> Evaluadores { get; set; }
        public virtual DbSet<Exposiciones> Exposiciones { get; set; }
        public virtual DbSet<Grupos> Grupos { get; set; }
        public virtual DbSet<Ponentes> Ponentes { get; set; }
        public virtual DbSet<Reactivos> Reactivos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=localhost;port=3306;user=root;database=expository", x => x.ServerVersion("10.4.11-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Calificaciones>(entity =>
            {
                entity.ToTable("calificaciones");

                entity.HasIndex(e => e.EvaluacionId)
                    .HasName("FK_Calificaciones_Evaluaciones_EvaluacionId");

                entity.HasIndex(e => e.EvaluadorId)
                    .HasName("fk_calificaciones_evaluadores1");

                entity.HasIndex(e => e.ReactivoId)
                    .HasName("FK_Calificaciones_reactivos_ReactivoId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Comentarios)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.EvaluacionId).HasColumnType("int(11)");

                entity.Property(e => e.EvaluadorId).HasColumnType("int(11)");

                entity.Property(e => e.ReactivoId).HasColumnType("int(11)");

                entity.Property(e => e.Valor).HasColumnType("int(11)");

                entity.HasOne(d => d.Evaluacion)
                    .WithMany(p => p.Calificaciones)
                    .HasForeignKey(d => d.EvaluacionId)
                    .HasConstraintName("FK_Calificaciones_Evaluaciones_EvaluacionId");

                entity.HasOne(d => d.Evaluador)
                    .WithMany(p => p.Calificaciones)
                    .HasForeignKey(d => d.EvaluadorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_calificaciones_evaluadores1");

                entity.HasOne(d => d.Reactivo)
                    .WithMany(p => p.Calificaciones)
                    .HasForeignKey(d => d.ReactivoId)
                    .HasConstraintName("FK_Calificaciones_reactivos_ReactivoId");
            });

            modelBuilder.Entity<Evaluaciones>(entity =>
            {
                entity.ToTable("evaluaciones");

                entity.HasIndex(e => e.ExposicionId)
                    .HasName("FK_Evaluaciones_Exposiciones_ExposicionId");

                entity.HasIndex(e => e.PonenteId)
                    .HasName("FK_Evaluaciones_Ponentes_PonenteId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.EvaluadorId)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.ExposicionId).HasColumnType("int(11)");

                entity.Property(e => e.PonenteId).HasColumnType("int(11)");

                entity.HasOne(d => d.Exposicion)
                    .WithMany(p => p.Evaluaciones)
                    .HasForeignKey(d => d.ExposicionId)
                    .HasConstraintName("FK_Evaluaciones_Exposiciones_ExposicionId");

                entity.HasOne(d => d.Ponente)
                    .WithMany(p => p.Evaluaciones)
                    .HasForeignKey(d => d.PonenteId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Evaluaciones_Ponentes_PonenteId");
            });

            modelBuilder.Entity<Evaluadores>(entity =>
            {
                entity.ToTable("evaluadores");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Apellidos)
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Nombres)
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<Exposiciones>(entity =>
            {
                entity.ToTable("exposiciones");

                entity.HasIndex(e => e.EvaluadorId)
                    .HasName("fk_exposiciones_evaluadores1");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.EvaluadorId).HasColumnType("int(11)");

                entity.Property(e => e.Titulo)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.HasOne(d => d.Evaluador)
                    .WithMany(p => p.Exposiciones)
                    .HasForeignKey(d => d.EvaluadorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_exposiciones_evaluadores1");
            });

            modelBuilder.Entity<Grupos>(entity =>
            {
                entity.ToTable("grupos");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Nombre)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<Ponentes>(entity =>
            {
                entity.ToTable("ponentes");

                entity.HasIndex(e => e.GrupoId)
                    .HasName("FK_Ponentes_Grupos_GrupoId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Apellidos)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.GrupoId).HasColumnType("int(11)");

                entity.Property(e => e.Matricula)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Nombres)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.HasOne(d => d.Grupo)
                    .WithMany(p => p.Ponentes)
                    .HasForeignKey(d => d.GrupoId)
                    .HasConstraintName("FK_Ponentes_Grupos_GrupoId");
            });

            modelBuilder.Entity<Reactivos>(entity =>
            {
                entity.ToTable("reactivos");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Titulo)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

﻿using System;
using System.Collections.Generic;

namespace Prueba2.Models.DataBase
{
    public partial class Reactivos
    {
        public Reactivos()
        {
            Calificaciones = new HashSet<Calificaciones>();
        }

        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<Calificaciones> Calificaciones { get; set; }
    }
}

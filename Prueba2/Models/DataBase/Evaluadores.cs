﻿using System;
using System.Collections.Generic;

namespace Prueba2.Models.DataBase
{
    public partial class Evaluadores
    {
        public Evaluadores()
        {
            Calificaciones = new HashSet<Calificaciones>();
            Exposiciones = new HashSet<Exposiciones>();
        }

        public int Id { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }

        public virtual ICollection<Calificaciones> Calificaciones { get; set; }
        public virtual ICollection<Exposiciones> Exposiciones { get; set; }
    }
}

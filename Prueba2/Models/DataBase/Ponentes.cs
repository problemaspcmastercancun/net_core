﻿using System;
using System.Collections.Generic;

namespace Prueba2.Models.DataBase
{
    public partial class Ponentes
    {
        public Ponentes()
        {
            Evaluaciones = new HashSet<Evaluaciones>();
        }

        public int Id { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Matricula { get; set; }
        public int GrupoId { get; set; }

        public virtual Grupos Grupo { get; set; }
        public virtual ICollection<Evaluaciones> Evaluaciones { get; set; }
    }
}

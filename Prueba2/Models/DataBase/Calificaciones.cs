﻿using System;
using System.Collections.Generic;

namespace Prueba2.Models.DataBase
{
    public partial class Calificaciones
    {
        public int Id { get; set; }
        public int Valor { get; set; }
        public string Comentarios { get; set; }
        public int ReactivoId { get; set; }
        public int? EvaluacionId { get; set; }
        public int EvaluadorId { get; set; }

        public virtual Evaluaciones Evaluacion { get; set; }
        public virtual Evaluadores Evaluador { get; set; }
        public virtual Reactivos Reactivo { get; set; }
    }
}

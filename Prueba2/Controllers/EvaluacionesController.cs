﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Prueba2.Models.DataBase;

namespace Prueba2.Controllers
{
    public class EvaluacionesController : Controller
    {
        private readonly expositoryContext _context;

        public EvaluacionesController(expositoryContext context)
        {
            _context = context;
        }

        // GET: Evaluaciones
        public async Task<IActionResult> Index()
        {
            var expositoryContext = _context.Evaluaciones.Include(e => e.Exposicion).Include(e => e.Ponente);
            return View(await expositoryContext.ToListAsync());
        }

        // GET: Evaluaciones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evaluaciones = await _context.Evaluaciones
                .Include(e => e.Exposicion)
                .Include(e => e.Ponente)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (evaluaciones == null)
            {
                return NotFound();
            }

            return View(evaluaciones);
        }

        // GET: Evaluaciones/Create
        public IActionResult Create()
        {
            ViewData["ExposicionId"] = new SelectList(_context.Exposiciones, "Id", "Id");
            ViewData["PonenteId"] = new SelectList(_context.Ponentes, "Id", "Id");
            return View();
        }

        // POST: Evaluaciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ExposicionId,EvaluadorId,PonenteId")] Evaluaciones evaluaciones)
        {
            if (ModelState.IsValid)
            {
                _context.Add(evaluaciones);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ExposicionId"] = new SelectList(_context.Exposiciones, "Id", "Id", evaluaciones.ExposicionId);
            ViewData["PonenteId"] = new SelectList(_context.Ponentes, "Id", "Id", evaluaciones.PonenteId);
            return View(evaluaciones);
        }

        // GET: Evaluaciones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evaluaciones = await _context.Evaluaciones.FindAsync(id);
            if (evaluaciones == null)
            {
                return NotFound();
            }
            ViewData["ExposicionId"] = new SelectList(_context.Exposiciones, "Id", "Id", evaluaciones.ExposicionId);
            ViewData["PonenteId"] = new SelectList(_context.Ponentes, "Id", "Id", evaluaciones.PonenteId);
            return View(evaluaciones);
        }

        // POST: Evaluaciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ExposicionId,EvaluadorId,PonenteId")] Evaluaciones evaluaciones)
        {
            if (id != evaluaciones.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(evaluaciones);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EvaluacionesExists(evaluaciones.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ExposicionId"] = new SelectList(_context.Exposiciones, "Id", "Id", evaluaciones.ExposicionId);
            ViewData["PonenteId"] = new SelectList(_context.Ponentes, "Id", "Id", evaluaciones.PonenteId);
            return View(evaluaciones);
        }

        // GET: Evaluaciones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evaluaciones = await _context.Evaluaciones
                .Include(e => e.Exposicion)
                .Include(e => e.Ponente)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (evaluaciones == null)
            {
                return NotFound();
            }

            return View(evaluaciones);
        }

        // POST: Evaluaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var evaluaciones = await _context.Evaluaciones.FindAsync(id);
            _context.Evaluaciones.Remove(evaluaciones);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EvaluacionesExists(int id)
        {
            return _context.Evaluaciones.Any(e => e.Id == id);
        }
    }
}

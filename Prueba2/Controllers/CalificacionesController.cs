﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Prueba2.Models.DataBase;

namespace Prueba2.Controllers
{
    public class CalificacionesController : Controller
    {
        private readonly expositoryContext _context;

        public CalificacionesController(expositoryContext context)
        {
            _context = context;
        }

        // GET: Calificaciones
        public async Task<IActionResult> Index()
        {
            var expositoryContext = _context.Calificaciones.Include(c => c.Evaluacion).Include(c => c.Evaluador).Include(c => c.Reactivo);
            return View(await expositoryContext.ToListAsync());
        }

        // GET: Calificaciones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var calificaciones = await _context.Calificaciones
                .Include(c => c.Evaluacion)
                .Include(c => c.Evaluador)
                .Include(c => c.Reactivo)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (calificaciones == null)
            {
                return NotFound();
            }

            return View(calificaciones);
        }

        // GET: Calificaciones/Create
        public IActionResult Create()
        {
            ViewData["EvaluacionId"] = new SelectList(_context.Evaluaciones, "Id", "Id");
            ViewData["EvaluadorId"] = new SelectList(_context.Evaluadores, "Id", "Id");
            ViewData["ReactivoId"] = new SelectList(_context.Reactivos, "Id", "Id");
            return View();
        }

        // POST: Calificaciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Valor,Comentarios,ReactivoId,EvaluacionId,EvaluadorId")] Calificaciones calificaciones)
        {
            if (ModelState.IsValid)
            {
                _context.Add(calificaciones);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EvaluacionId"] = new SelectList(_context.Evaluaciones, "Id", "Id", calificaciones.EvaluacionId);
            ViewData["EvaluadorId"] = new SelectList(_context.Evaluadores, "Id", "Id", calificaciones.EvaluadorId);
            ViewData["ReactivoId"] = new SelectList(_context.Reactivos, "Id", "Id", calificaciones.ReactivoId);
            return View(calificaciones);
        }

        // GET: Calificaciones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var calificaciones = await _context.Calificaciones.FindAsync(id);
            if (calificaciones == null)
            {
                return NotFound();
            }
            ViewData["EvaluacionId"] = new SelectList(_context.Evaluaciones, "Id", "Id", calificaciones.EvaluacionId);
            ViewData["EvaluadorId"] = new SelectList(_context.Evaluadores, "Id", "Id", calificaciones.EvaluadorId);
            ViewData["ReactivoId"] = new SelectList(_context.Reactivos, "Id", "Id", calificaciones.ReactivoId);
            return View(calificaciones);
        }

        // POST: Calificaciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Valor,Comentarios,ReactivoId,EvaluacionId,EvaluadorId")] Calificaciones calificaciones)
        {
            if (id != calificaciones.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(calificaciones);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CalificacionesExists(calificaciones.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EvaluacionId"] = new SelectList(_context.Evaluaciones, "Id", "Id", calificaciones.EvaluacionId);
            ViewData["EvaluadorId"] = new SelectList(_context.Evaluadores, "Id", "Id", calificaciones.EvaluadorId);
            ViewData["ReactivoId"] = new SelectList(_context.Reactivos, "Id", "Id", calificaciones.ReactivoId);
            return View(calificaciones);
        }

        // GET: Calificaciones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var calificaciones = await _context.Calificaciones
                .Include(c => c.Evaluacion)
                .Include(c => c.Evaluador)
                .Include(c => c.Reactivo)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (calificaciones == null)
            {
                return NotFound();
            }

            return View(calificaciones);
        }

        // POST: Calificaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var calificaciones = await _context.Calificaciones.FindAsync(id);
            _context.Calificaciones.Remove(calificaciones);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CalificacionesExists(int id)
        {
            return _context.Calificaciones.Any(e => e.Id == id);
        }
    }
}

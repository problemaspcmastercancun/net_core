﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Prueba2.Models.DataBase;

namespace Prueba2.Controllers
{
    public class EvaluadoresController : Controller
    {
        private readonly expositoryContext _context;

        public EvaluadoresController(expositoryContext context)
        {
            _context = context;
        }

        // GET: Evaluadores
        public async Task<IActionResult> Index()
        {
            return View(await _context.Evaluadores.ToListAsync());
        }

        // GET: Evaluadores/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evaluadores = await _context.Evaluadores
                .FirstOrDefaultAsync(m => m.Id == id);
            if (evaluadores == null)
            {
                return NotFound();
            }

            return View(evaluadores);
        }

        // GET: Evaluadores/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Evaluadores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombres,Apellidos")] Evaluadores evaluadores)
        {
            if (ModelState.IsValid)
            {
                _context.Add(evaluadores);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(evaluadores);
        }

        // GET: Evaluadores/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evaluadores = await _context.Evaluadores.FindAsync(id);
            if (evaluadores == null)
            {
                return NotFound();
            }
            return View(evaluadores);
        }

        // POST: Evaluadores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombres,Apellidos")] Evaluadores evaluadores)
        {
            if (id != evaluadores.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(evaluadores);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EvaluadoresExists(evaluadores.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(evaluadores);
        }

        // GET: Evaluadores/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evaluadores = await _context.Evaluadores
                .FirstOrDefaultAsync(m => m.Id == id);
            if (evaluadores == null)
            {
                return NotFound();
            }

            return View(evaluadores);
        }

        // POST: Evaluadores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var evaluadores = await _context.Evaluadores.FindAsync(id);
            _context.Evaluadores.Remove(evaluadores);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EvaluadoresExists(int id)
        {
            return _context.Evaluadores.Any(e => e.Id == id);
        }
    }
}

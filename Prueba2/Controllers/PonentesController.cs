﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Prueba2.Models.DataBase;

namespace Prueba2.Controllers
{
    public class PonentesController : Controller
    {
        private readonly expositoryContext _context;

        public PonentesController(expositoryContext context)
        {
            _context = context;
        }

        // GET: Ponentes
        public async Task<IActionResult> Index()
        {
            var expositoryContext = _context.Ponentes.Include(p => p.Grupo);
            return View(await expositoryContext.ToListAsync());
        }

        // GET: Ponentes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ponentes = await _context.Ponentes
                .Include(p => p.Grupo)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (ponentes == null)
            {
                return NotFound();
            }

            return View(ponentes);
        }

        // GET: Ponentes/Create
        public IActionResult Create()
        {
            ViewData["GrupoId"] = new SelectList(_context.Grupos, "Id", "Id");
            return View();
        }

        // POST: Ponentes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombres,Apellidos,Matricula,GrupoId")] Ponentes ponentes)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ponentes);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["GrupoId"] = new SelectList(_context.Grupos, "Id", "Id", ponentes.GrupoId);
            return View(ponentes);
        }

        // GET: Ponentes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ponentes = await _context.Ponentes.FindAsync(id);
            if (ponentes == null)
            {
                return NotFound();
            }
            ViewData["GrupoId"] = new SelectList(_context.Grupos, "Id", "Id", ponentes.GrupoId);
            return View(ponentes);
        }

        // POST: Ponentes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombres,Apellidos,Matricula,GrupoId")] Ponentes ponentes)
        {
            if (id != ponentes.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ponentes);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PonentesExists(ponentes.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GrupoId"] = new SelectList(_context.Grupos, "Id", "Id", ponentes.GrupoId);
            return View(ponentes);
        }

        // GET: Ponentes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ponentes = await _context.Ponentes
                .Include(p => p.Grupo)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (ponentes == null)
            {
                return NotFound();
            }

            return View(ponentes);
        }

        // POST: Ponentes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ponentes = await _context.Ponentes.FindAsync(id);
            _context.Ponentes.Remove(ponentes);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PonentesExists(int id)
        {
            return _context.Ponentes.Any(e => e.Id == id);
        }
    }
}
